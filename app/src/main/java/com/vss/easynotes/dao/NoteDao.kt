package com.vss.easynotes.dao

import androidx.room.*
import com.vss.easynotes.entities.Note

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
   suspend fun insertNote(note: Note)

    @Query("select * from notes order by sortBy desc")
    suspend fun getAllNotes():List<Note>

    @Update
    suspend fun updateNote(note: Note)

    @Delete
    suspend fun deleteNote(note: Note)

}