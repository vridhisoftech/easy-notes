package com.vss.easynotes.activities

import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.vss.easynotes.AlarmReceiver
import com.vss.easynotes.R
import com.vss.easynotes.adapters.NotesAdapter
import com.vss.easynotes.database.NoteDatabase
import com.vss.easynotes.entities.Note
import com.vss.easynotes.util.RecyclerItemClickListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.empty_view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var job: Job
    var mActionMode: ActionMode? = null
    var contextMenu: Menu? = null
    var multiSelectAdapter: NotesAdapter? = null
    var isMultiSelect = false
    private var noteList: ArrayList<Note> = ArrayList()
    var multiSelectNoteList: ArrayList<Note> = ArrayList()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main
    val requestCodeAddNote = 1
    val requestCodeUpdateNote = 2
    private val sharedPrefName: String = "MyFirstLogging"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        job = Job()
        supportActionBar!!.title = resources.getString(R.string.app_name)
        img_add_note_main.setOnClickListener {
            val intent = Intent(this, CreateNoteActivity::class.java)
            startActivityForResult(intent, requestCodeAddNote)
        }


        val sharedPref = getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)
        val firstTimeLogging = sharedPref.getBoolean("firstTime", false)

        if (firstTimeLogging) {
            getNotes()
        } else {
            val editor2: SharedPreferences.Editor = sharedPref.edit()
            editor2.putBoolean("firstTime", true)
            editor2.apply()
            prepareData()
        }

        inputSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    multiSelectAdapter!!.filter.filter(s.toString())
                } catch (e:NullPointerException) {
                     e.printStackTrace()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (multiSelectAdapter?.itemCount == 0){
                    empty.visibility = View.VISIBLE
                }else {
                    empty.visibility = View.GONE
                }
            }
        })

        getNotes()
    }

    private fun prepareData() {
        val note1 = Note()
        note1.title = "Image Note"
        note1.noteText = "Welcome to EasyNote"
        note1.dateTime =
            SimpleDateFormat("EEEE , dd MMMM yyyy HH:mm a", Locale.getDefault()).format(Date())
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.banner)
        val imageSource = encodeTobase64(bitmap)
        note1.imageTemp = imageSource
        note1.color = "#EBBECB"
        note1.reminder = ""
        note1.reminderId = 0
        val note2 = Note()
        note2.title = "Text Note"
        note2.noteText = "Simple Text Note"
        note2.dateTime =
            SimpleDateFormat("EEEE , dd MMMM yyyy HH:mm a", Locale.getDefault()).format(Date())
        note2.color = "#EBBECB"
        note2.reminder = ""
        note2.reminderId = 0
        val note3 = Note()
        note3.title = "Colorful Note"
        note3.noteText = "Change Note Colors"
        note3.dateTime =
            SimpleDateFormat("EEEE , dd MMMM yyyy HH:mm a", Locale.getDefault()).format(Date())
        note3.color = "#BB86FC"
        note3.reminder = ""
        note3.reminderId = 0
        val note4 = Note()
        note4.title = "Many Other Type Notes"
        note4.noteText =
            "You can add some more feature from Menu Option like- \n1. Add Image Note\n2. Add Reminder " +
                    "\n3. Share Notes\n4. Save Note to local storage"
        note4.dateTime =
            SimpleDateFormat("EEEE , dd MMMM yyyy HH:mm a", Locale.getDefault()).format(Date())
        note4.color = "#EBBECB"
        note4.reminder = ""
        note4.reminderId = 0
        launch {

            let {
                NoteDatabase(applicationContext).getNoteDao().insertNote(note2)
                NoteDatabase(applicationContext).getNoteDao().insertNote(note3)
                NoteDatabase(applicationContext).getNoteDao().insertNote(note4)
                NoteDatabase(applicationContext).getNoteDao().insertNote(note1)
                getNotes()
            }
        }
    }

    private fun encodeTobase64(image: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 80, baos)
        val b = baos.toByteArray()
        val imageEncoded =
            Base64.encodeToString(b, Base64.DEFAULT)
        Log.d("Image Log:", imageEncoded)
        return imageEncoded
    }

    private fun getNotes() {
        notesRecyclerView!!.setHasFixedSize(false)
        notesRecyclerView!!.layoutManager =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        launch {
            this.let {
                val note = NoteDatabase(applicationContext).getNoteDao().getAllNotes()
                noteList = note as ArrayList<Note>
                multiSelectAdapter = NotesAdapter(noteList, multiSelectNoteList, this@MainActivity)
                notesRecyclerView!!.adapter = multiSelectAdapter
            }
        }
        notesRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                notesRecyclerView,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        if (isMultiSelect)
                            multiSelect(position)
                    }

                    override fun onItemLongClick(view: View?, position: Int) {
                        if (!isMultiSelect) {
                            multiSelectNoteList = ArrayList()
                            isMultiSelect = true
                            if (mActionMode == null) {

                                mActionMode = startActionMode(mActionModeCallback)
                                multiSelect(position)
                            }
                        }
                        Log.v("PositionSelected", "Pos: $position")
                    }
                })
        )

    }

    fun multiSelect(position: Int) {
        if (mActionMode != null) {
            try {
                Log.v("Sizes", "" + multiSelectNoteList.size + " " + noteList.size)
                if (multiSelectNoteList.contains(noteList[position]))
                    multiSelectNoteList.remove(noteList[position])
                else multiSelectNoteList.add(noteList[position])

                if (multiSelectNoteList.size > 0 && noteList.size > multiSelectNoteList.size) {
                    mActionMode!!.title = "" + multiSelectNoteList.size

                    Log.v("all n0t selected", "" + multiSelectNoteList.size + " " + noteList.size)
                    val allSelected = mActionMode!!.menu.getItem(0)
                    val unSelectAll = mActionMode!!.menu.getItem(1)
                    allSelected.isVisible = true
                    unSelectAll.isVisible = false

                } else if (multiSelectNoteList.size == noteList.size) {
                    mActionMode!!.title = "" + multiSelectNoteList.size

                    val allSelected = mActionMode!!.menu.getItem(0)
                    val unSelectAll = mActionMode!!.menu.getItem(1)
                    Log.v("MenuItems", "$allSelected $unSelectAll")
                    Log.v("all  selected", "" + multiSelectNoteList.size + " " + noteList.size)

                    allSelected.isVisible = false
                    unSelectAll.isVisible = true
                } else {
                    Log.v("MenuItems", "Here" + multiSelectNoteList.size)

                    mActionMode!!.title = ""
                    mActionMode!!.finish()
                }
                refreshAdapter()
            } catch (e: ArrayIndexOutOfBoundsException) {
                Log.v("ArrayIndexError:", " $e")
            }
        }

    }

    fun selectAll() {
        if (mActionMode != null) {
            try {
                Log.v("Sizes", "" + multiSelectNoteList.size + " " + noteList.size)
                multiSelectNoteList.clear()
                multiSelectNoteList.addAll(noteList)
                if (multiSelectNoteList.size > 0)
                    mActionMode!!.title = "" + multiSelectNoteList.size
                else {
                    mActionMode!!.title = ""
                    mActionMode!!.finish()
                }
                refreshAdapter()
            } catch (e: ArrayIndexOutOfBoundsException) {
                Log.v("ArrayIndexError:", " $e")
            }
        }

    }

    fun unSelectAll() {
        if (mActionMode != null) {
            try {
                Log.v("Sizes", "" + multiSelectNoteList.size + " " + noteList.size)

                multiSelectNoteList.removeAll(noteList)
                if (multiSelectNoteList.size > 0)
                    mActionMode!!.title = "" + multiSelectNoteList.size
                else {
                    mActionMode!!.title = ""
                    mActionMode!!.finish()
                }
                refreshAdapter()
            } catch (e: ArrayIndexOutOfBoundsException) {
                Log.v("ArrayIndexError:", " $e")
            }
        }

    }

    fun refreshAdapter() {
        multiSelectAdapter!!.selected_usersList = multiSelectNoteList
        multiSelectAdapter!!.noteFilterList = noteList
        multiSelectAdapter!!.notifyDataSetChanged()
    }

    private val mActionModeCallback: ActionMode.Callback =
        object : ActionMode.Callback {
            override fun onCreateActionMode(
                mode: ActionMode, menu: Menu
            ): Boolean { // Inflate a menu resource providing context menu items
                val inflater = mode.menuInflater
                inflater.inflate(R.menu.menu, menu)
                contextMenu = menu
                supportActionBar!!.hide()
                img_add_note_main!!.visibility = View.GONE
                inputSearch!!.isEnabled = false
                val itemSelect = mode.menu.getItem(0)
                itemSelect.isVisible = true
                val itemUnSelect = mode.menu.getItem(1)
                itemUnSelect.isVisible = false
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                return false // Return false if nothing is done
            }

            override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {

                return when (item.itemId) {

                    R.id.selectAll -> {
                        val itemUnSelect = mode.menu.getItem(1)
                        itemUnSelect.isVisible = true
                        val itemSelect = mode.menu.getItem(0)
                        itemSelect.isVisible = false
                        selectAll()
                        true
                    }
                    R.id.unSelectAll -> {
                        unSelectAll()
                        true
                    }
                    R.id.delete -> {
                        showCustomDialog(multiSelectNoteList)
                        true
                    }


                    else -> false
                }
            }

            override fun onDestroyActionMode(mode: ActionMode) {
                mActionMode = null
                isMultiSelect = false
                multiSelectNoteList = ArrayList()
                refreshAdapter()
                supportActionBar!!.show()
                img_add_note_main!!.visibility = View.VISIBLE
                inputSearch!!.isEnabled = true

            }
        }

    private fun showCustomDialog(multiSelect: ArrayList<Note>) {
        val viewGroup = findViewById<ViewGroup>(android.R.id.content)
        val dialogView = LayoutInflater.from(this).inflate(R.layout.layout_delete, viewGroup, false)
        val builder = AlertDialog.Builder(this)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        if (alertDialog.window != null) {
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(0))
        }
        dialogView.findViewById<TextView>(R.id.textDeleteNote).setOnClickListener {
            if (multiSelect.size > 0) {
                launch {
                    for (i in 0 until multiSelect.size) {
                        if (multiSelect[i].reminder!!.length>1){
                       val alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
                        val myIntent = Intent(applicationContext, AlarmReceiver::class.java)
                        myIntent.putExtra("notificationId", 1)
                        myIntent.putExtra("todo", multiSelect[i].title)
                        val alarmIntent = PendingIntent.getBroadcast(
                            applicationContext, multiSelect[i].reminderId!!, myIntent,
                            0
                        )
                            alarm.cancel(alarmIntent)
                        }
                        NoteDatabase(applicationContext).getNoteDao()
                            .deleteNote((multiSelect[i]))
                        multiSelectAdapter!!.notifyDataSetChanged()

                    }
                }
                for (i in multiSelectNoteList.indices) noteList.remove(multiSelectNoteList[i])

                multiSelectAdapter!!.notifyDataSetChanged()

                if (mActionMode != null) {
                    mActionMode!!.finish()
                }
                refreshAdapter()
                multiSelectAdapter!!.notifyDataSetChanged()
                Toast.makeText(applicationContext, "Deleted", Toast.LENGTH_SHORT).show()
                alertDialog.dismiss()
            }
        }

        dialogView.findViewById<TextView>(R.id.cancelDelete).setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestCodeAddNote && resultCode == RESULT_OK) {
            getNotes()
            refreshAdapter()
        }
        if (requestCode == requestCodeUpdateNote && resultCode == RESULT_OK) {
            getNotes()
            refreshAdapter()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

}
