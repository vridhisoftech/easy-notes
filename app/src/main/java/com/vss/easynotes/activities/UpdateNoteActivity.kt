package com.vss.easynotes.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.iceteck.silicompressorr.SiliCompressor
import com.vss.easynotes.AlarmReceiver
import com.vss.easynotes.R
import com.vss.easynotes.database.NoteDatabase
import com.vss.easynotes.entities.Note
import kotlinx.android.synthetic.main.activity_create_note.*
import kotlinx.android.synthetic.main.layout_miscellaneous.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class UpdateNoteActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main
    private var selectedNoteColor: String? = null
    private var viewSubtitle: View? = null
    private val REQUEST_PERMISSION = 100
    private val REQUEST_PERMISSIONFile = 101
    private var selectedMediaPath: String? = null
    var selectedTempPath: String? = null
    var alreadyAdded: Note? = null
    var selectedDate: String? = null
    var selectedTime: String? = null
    var selectedTimeRaw: String? = null
    var targetedDate: String? = null
    var selectedHour: Int? = null
    var alarmIntent: PendingIntent? = null
    var alarm: AlarmManager? = null
    var myIntent: Intent? = null
    var alarmId: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_note)
        viewSubtitle = findViewById(R.id.viewSubtitleIndicator)
        job = Job()
        alarmId = System.currentTimeMillis().toInt()
        img_back.setOnClickListener {
            it?.hideKeyboard()
            onBackPressed()
        }


        selectedNoteColor = "#EBBECB"
        selectedMediaPath = null
        selectedTempPath = null
        targetedDate = ""
        selectedTime = ""
        ll_add_reminder_layout.setOnClickListener {
            setAlarm()
        }
        imgResetAlarm.setOnClickListener {
            setAlarm()
        }
        text_reminder.setOnClickListener {
            setAlarm()
        }
        img_save.setOnClickListener {
            it?.hideKeyboard()
            saveNote()
        }

        val viewing = intent.extras!!.getBoolean("isViewOrUpdated", false)
        if (viewing) {
            alreadyAdded = intent.getSerializableExtra("note") as Note?
            ll_share_layout.visibility = View.VISIBLE
            ll_save_layout.visibility = View.VISIBLE
            setViewOrUpdated()
        }

        ll_share_layout.setOnClickListener {
            shareNote()
        }
        ll_save_layout.setOnClickListener {
            saveToFile()
        }
        initBottomSheet()
        setSubtitleIndicator()

        alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        myIntent = Intent(applicationContext, AlarmReceiver::class.java)
        myIntent!!.putExtra("notificationId", 1)
        myIntent!!.putExtra("todo", edt_noteTitle.text.toString())
        alarmIntent = PendingIntent.getBroadcast(
            applicationContext, alreadyAdded!!.reminderId!!, myIntent,
            0
        )

        img_delete_reminder.setOnClickListener {
            alarm!!.cancel(alarmIntent)
            ll_reminder_layout.visibility = View.GONE
            targetedDate = ""
            selectedTime = ""
            text_reminder.text = ""
        }

        deleteNoteImage.setOnClickListener {
            noteImage.setImageBitmap(null)
            noteImage.visibility = View.GONE
            deleteNoteImage.visibility = View.GONE
            selectedMediaPath = null
            selectedTempPath = null
        }
    }

    private fun shareNote() {
        val title = edt_noteTitle!!.text.toString()
        val note = edt_note.text.toString()
        when {
            title.isEmpty() -> {
                Toast.makeText(this, "Title is required", Toast.LENGTH_SHORT).show()
                return
            }

            note.isEmpty() -> {
                Toast.makeText(this, "Note is required", Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(
                    Intent.EXTRA_TEXT, "Title: $title\n\n Note: $note"
                )
                startActivity(Intent.createChooser(shareIntent, "Share..."))
            }
        }
    }

    private fun saveToFile() {
        setupForReadAndWritePermissions()
    }

    private fun setAlarm() {
        datePicker()
    }

    @SuppressLint("SimpleDateFormat")
    private fun datePicker() {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                selectedDate = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)

                val dateFormat = SimpleDateFormat("dd-M-yyyy")
                var sourceDate: Date? = null
                try {
                    sourceDate = dateFormat.parse(selectedDate!!)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                val targetFormat = SimpleDateFormat("MMMM dd")
                targetedDate = targetFormat.format(sourceDate!!)

                if (selectedDate != null) {
                    timePicker()
                }
            }, mYear, mMonth, mDay
        )
        datePickerDialog.datePicker.minDate = Date().time
        datePickerDialog.show()
    }

    @SuppressLint("SimpleDateFormat")
    private fun timePicker() {
        var am_pm: String? = null
        val mCurrentTime = Calendar.getInstance()
        val hour = mCurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mCurrentTime.get(Calendar.MINUTE)
        if (mCurrentTime.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM"
        else if (mCurrentTime.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM"
        val timePickerDialog = TimePickerDialog(
            this,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                selectedTimeRaw = "$hourOfDay:$minute"
                when {
                    hourOfDay == 0 -> {
                        selectedHour = hourOfDay + 12
                        am_pm = "AM"
                    }
                    hourOfDay == 12 -> {
                        selectedHour = hourOfDay
                        am_pm = "PM"
                    }
                    hourOfDay > 12 -> {
                        selectedHour = hourOfDay - 12
                        am_pm = "PM"
                    }
                    else -> {
                        am_pm = "AM"
                    }
                }
                selectedTime = "$selectedHour:$minute:00 $am_pm"
                if (selectedTime != null) {

                    ll_reminder_layout.visibility = View.VISIBLE
                    text_reminder.text = "$targetedDate, $selectedTime"
                }
            }, hour, minute, false
        )
        timePickerDialog.show()

    }

    @SuppressLint("SimpleDateFormat")
    private fun convertDateTimeToMillis() {
        val date = "$selectedDate $selectedTimeRaw"//"2014-11-25 14:30"
        val sdf = SimpleDateFormat("dd-M-yyyy HH:mm")
        var testDate: Date? = null

        alarmIntent = PendingIntent.getBroadcast(
            this, alreadyAdded!!.reminderId!!,
            myIntent, 0
        )
        alarm = getSystemService(ALARM_SERVICE) as AlarmManager
        try {
            testDate = sdf.parse(date)
            alarm!!.set(AlarmManager.RTC, testDate.time, alarmIntent)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setViewOrUpdated() {
        edt_noteTitle.setText(alreadyAdded!!.title)
        edt_note.setText(alreadyAdded!!.noteText)
        text_date_time.text = alreadyAdded!!.dateTime
        Log.v("ReminderId", alreadyAdded!!.reminder!! + "  " + alreadyAdded!!.reminderId!!)

        if (alreadyAdded!!.reminder!!.length > 1 && alreadyAdded!!.reminder!!.trim().isNotEmpty()) {
            Log.v("Reminder", alreadyAdded!!.reminder!!)

            text_reminder.text = alreadyAdded!!.reminder
            ll_reminder_layout.visibility = View.VISIBLE
        } else {
            ll_reminder_layout.visibility = View.GONE
        }
        if (alreadyAdded!!.imagePath != null && alreadyAdded!!.imagePath!!.trim()
                .isNotEmpty() && alreadyAdded!!.imageTemp == null
        ) {
            noteImage.setImageBitmap(BitmapFactory.decodeFile(alreadyAdded!!.imagePath))
            noteImage.visibility = View.VISIBLE
            deleteNoteImage.visibility = View.VISIBLE
            selectedMediaPath = alreadyAdded!!.imagePath
        } else if (alreadyAdded!!.imagePath == null && alreadyAdded!!.imageTemp != null) {
            val bmp = decodeBase64(alreadyAdded!!.imageTemp!!)
            if (bmp != null) {
                noteImage.setImageBitmap(bmp)
                noteImage.visibility = View.VISIBLE
                deleteNoteImage.visibility = View.VISIBLE
                selectedTempPath = alreadyAdded!!.imageTemp
                selectedMediaPath = null
            }
        } else {
            noteImage.visibility = View.GONE
        }
    }

    private fun decodeBase64(input: String?): Bitmap? {
        val decodedByte = Base64.decode(input, 0)
        return BitmapFactory
            .decodeByteArray(decodedByte, 0, decodedByte.size)
    }

    private fun saveNote() {
        val title = edt_noteTitle.text.toString()
        val note = edt_note.text.toString()
        if (title.trim().isEmpty()) {
            Toast.makeText(this, "Title Required", Toast.LENGTH_SHORT).show()
            return
        }

        if (note.trim().isEmpty()) {
            Toast.makeText(this, "Note Required", Toast.LENGTH_SHORT).show()
            return
        }

        if (selectedMediaPath != null) {
            selectedTempPath = null
        }
        text_date_time.text =
            SimpleDateFormat("EEEE , dd MMMM yyyy HH:mm a", Locale.getDefault()).format(Date())

        launch {

            let {
                val mNote = Note()
                mNote.title = title
                mNote.noteText = note
                mNote.dateTime = text_date_time.text.toString()
                mNote.title = title
                mNote.color = selectedNoteColor
                mNote.imagePath = selectedMediaPath
                mNote.imageTemp = selectedTempPath
                mNote.reminder = text_reminder.text.toString()
                mNote.reminderId = alreadyAdded!!.reminderId
                mNote.millis = alarmId

                if (text_reminder.text.toString().length > 1) {
                    convertDateTimeToMillis()
                }
                if (alreadyAdded == null) {
                    NoteDatabase(applicationContext).getNoteDao().insertNote(mNote)
                    Toast.makeText(applicationContext, "Saved", Toast.LENGTH_SHORT).show()
                } else {
                    mNote.id = alreadyAdded!!.id
                    NoteDatabase(applicationContext).getNoteDao().updateNote(mNote)
                    Toast.makeText(applicationContext, "Updated", Toast.LENGTH_SHORT).show()
                }
                setResult(Activity.RESULT_OK)
                finish()

            }
        }
    }

    private fun showCustomDialog() {
        val viewGroup = findViewById<ViewGroup>(android.R.id.content)
        val dialogView = LayoutInflater.from(this).inflate(R.layout.layout_delete, viewGroup, false)
        val builder = AlertDialog.Builder(this)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        if (alertDialog.window != null) {
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(0))
        }
        dialogView.findViewById<TextView>(R.id.textDeleteNote).setOnClickListener {
            launch {
                NoteDatabase(applicationContext).getNoteDao().deleteNote(alreadyAdded!!)
                alertDialog.dismiss()
                setResult(Activity.RESULT_OK)
                finish()
            }
        }

        dialogView.findViewById<TextView>(R.id.cancelDelete).setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    private fun initBottomSheet() {
        val chooseTheme = findViewById<LinearLayout>(R.id.ll_miscellaneous_layout)
        bottomSheetBehavior = BottomSheetBehavior.from(chooseTheme)
        bottomSheetBehavior.peekHeight = (60 * resources.displayMetrics.density).toInt()
        chooseTheme.findViewById<TextView>(R.id.choose_theme).setOnClickListener {
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//                val view = this.currentFocus
//                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                imm.hideSoftInputFromWindow(view!!.windowToken, 0)
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        ll_add_image_layout.setOnClickListener {
            setupPermissions()
        }
        ll_delete_layout.visibility = View.VISIBLE
        ll_delete_layout.setOnClickListener {
            showCustomDialog()
        }
        viewColor1.setOnClickListener {
            selectedNoteColor = "#EBBECB"
            imgColor1.setImageResource(R.drawable.ic_done)
            imgColor2.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor5.setImageResource(0)
            imgColor6.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor2.setOnClickListener {
            selectedNoteColor = "#fdbe3b"
            imgColor2.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor5.setImageResource(0)
            imgColor6.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor3.setOnClickListener {
            selectedNoteColor = "#ff4842"
            imgColor3.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor2.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor5.setImageResource(0)
            imgColor6.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor4.setOnClickListener {
            selectedNoteColor = "#BB86FC"
            imgColor4.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor2.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor5.setImageResource(0)
            imgColor6.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor5.setOnClickListener {
            selectedNoteColor = "#83624C"
            imgColor5.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor2.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor6.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor6.setOnClickListener {
            selectedNoteColor = "#0c7871"
            imgColor6.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor2.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor5.setImageResource(0)
            setSubtitleIndicator()
        }
        if (alreadyAdded != null && alreadyAdded!!.color != null && alreadyAdded!!.color!!.trim()
                .isNotEmpty()
        ) {
            when (alreadyAdded!!.color) {
                "#fdbe3b" -> {
                    viewColor2.performClick()
                }
                "#ff4842" -> {
                    viewColor3.performClick()
                }
                "#BB86FC" -> {
                    viewColor4.performClick()
                }
                "#83624C" -> {
                    viewColor5.performClick()
                }
                "#0c7871" -> {
                    viewColor6.performClick()
                }
            }
        }
    }

    private fun setSubtitleIndicator() {
        val gradientDrawable = viewSubtitle!!.background as GradientDrawable
        gradientDrawable.setColor(Color.parseColor(selectedNoteColor))
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_PERMISSION
            )
        } else {
            selectImage()
        }
    }

    private fun setupForReadAndWritePermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_PERMISSIONFile
            )
        } else {
            saveYourFile()
        }
    }

    private fun saveYourFile() {
        val title = edt_noteTitle!!.text.toString()
        val note = edt_note.text.toString()
        when {
            title.isEmpty() -> {
                Toast.makeText(this, "Title is required", Toast.LENGTH_SHORT).show()
                return
            }
            note.isEmpty() -> {
                Toast.makeText(this, "Note is required", Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                try {
                    val path = Environment.getExternalStorageDirectory()

                    val fileDirSave = File("$path/MyEasyNotes")
                    fileDirSave.mkdir()

                    val fileName = "${title}.txt"

                    val file = File(fileDirSave, fileName)

                    val fw = FileWriter(file.absoluteFile)
                    val bw = BufferedWriter(fw)
                    bw.write("Title: $title \nNote: $note")
                    bw.close()
                    Toast.makeText(this, "File is saved to : $fileDirSave", Toast.LENGTH_LONG)
                        .show()
                } catch (e: java.lang.Exception) {
                    Log.v("File Saved", "" + e.message)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSION -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("TAG", "Permission has been denied by user")
                } else {
                    selectImage()
                }
            }
            REQUEST_PERMISSIONFile -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("TAG", "Permission has been denied by user")
                } else {
                    saveYourFile()
                }
            }
        }
    }

    private fun selectImage() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), 2)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 2) {
                if (data != null) {
                    deleteNoteImage.visibility = View.VISIBLE
                    val image = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, image)
                    noteImage.setImageBitmap(bitmap)
                    noteImage.visibility = View.VISIBLE
                    val projection = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = contentResolver.query(image!!, projection, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()

                    val columnIndex = cursor.getColumnIndex(projection[0])
                    val mediaPath = cursor.getString(columnIndex)
                    val fileDirSave = File(
                        this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                            .toString() + "/EazyNotes_Assets"
                    )
                    val filePath = SiliCompressor.with(this).compress(mediaPath, fileDirSave)
                    selectedMediaPath = filePath
                    cursor.close()

                }
            }
        }
    }

    fun View.hideKeyboard() {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
