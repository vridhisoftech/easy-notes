package com.vss.easynotes.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.recyclerview.widget.RecyclerView
import com.vss.easynotes.R
import com.vss.easynotes.activities.MainActivity
import com.vss.easynotes.activities.UpdateNoteActivity
import com.vss.easynotes.entities.Note
import kotlinx.android.synthetic.main.all_my_notes.view.*
import kotlinx.android.synthetic.main.empty_view.view.*

class NotesAdapter(
    var notes: List<Note>,
    private var selectedNote: List<Note>,
    private var mContext: MainActivity
) : RecyclerView.Adapter<NotesAdapter.NoteViewHolder>(), Filterable {
    var noteFilterList: List<Note> = ArrayList()
    var context: Context? = null
    var selected_usersList: List<Note> = ArrayList()

    init {
        this.noteFilterList = notes as ArrayList<Note>
        this.selected_usersList = selectedNote as ArrayList<Note>
        this.context = mContext
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.all_my_notes, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return noteFilterList.size
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {

        holder.view.note_title.text = noteFilterList[position].title
        holder.view.note_text.text = noteFilterList[position].noteText
        holder.view.note_date_time.text = noteFilterList[position].dateTime

        if (noteFilterList[position].reminder!!.length > 1) {
            holder.itemView.ll_reminder_layout.visibility = View.VISIBLE
            holder.itemView.text_reminder.text = noteFilterList[position].reminder
        } else {
            holder.itemView.ll_reminder_layout.visibility = View.GONE
        }

        holder.itemView.ll_note_layout.setOnClickListener {
            val intent = Intent(context, UpdateNoteActivity::class.java)
            intent.putExtra("isViewOrUpdated", true)
            intent.putExtra("note", noteFilterList[position])
            startActivityForResult(context as AppCompatActivity, intent, 2, null)
        }

        val gradientDrawable = holder.view.ll_note_layout.background as GradientDrawable
        if (noteFilterList[position].color != null) {
            gradientDrawable.setColor(Color.parseColor(noteFilterList[position].color))
        } else {
            gradientDrawable.setColor(Color.parseColor("#333333"))
        }
        if (noteFilterList[position].imagePath != null && noteFilterList[position].imageTemp == null) {
            holder.itemView.noteImageAdded.setImageBitmap(BitmapFactory.decodeFile(noteFilterList[position].imagePath))
            holder.itemView.noteImageAdded.visibility = View.VISIBLE
        } else if (noteFilterList[position].imagePath == null && noteFilterList[position].imageTemp != null) {
            val bmp = decodeBase64(noteFilterList[position].imageTemp!!)
            if (bmp != null) {
                holder.itemView.noteImageAdded.setImageBitmap(bmp)
                holder.itemView.noteImageAdded.visibility = View.VISIBLE
            }
        } else {
            holder.itemView.noteImageAdded.visibility = View.GONE
        }
        if (selected_usersList.isNotEmpty()) {
            holder.view.rl_selected_layout.visibility = View.VISIBLE

            if (selected_usersList.contains(noteFilterList[position])) {
                holder.view.selectImage.visibility = View.VISIBLE
                holder.view.unSelectImage.visibility = View.GONE

            } else {
                holder.view.selectImage.visibility = View.GONE
                holder.view.unSelectImage.visibility = View.VISIBLE
            }
        } else {
            holder.view.rl_selected_layout.visibility = View.GONE
        }
    }

    private fun decodeBase64(input: String?): Bitmap? {
        val decodedByte = Base64.decode(input, 0)
        return BitmapFactory
            .decodeByteArray(decodedByte, 0, decodedByte.size)
    }

    class NoteViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                noteFilterList = if (charString.isEmpty()) {
                    notes
                } else {
                    val filteredList: ArrayList<Note> = ArrayList()
                    for (row in notes) {
                        if (row.title!!.toLowerCase().contains(charString.toLowerCase()) ||
                            row.noteText!!.toLowerCase()
                                .contains(charSequence.toString().toLowerCase())
                        )
                            filteredList.add(row)
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = noteFilterList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                noteFilterList = filterResults.values as List<Note>
                notifyDataSetChanged()
            }
        }
    }


}