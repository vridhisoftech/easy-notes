package com.vss.easynotes.entities

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "notes")
 class Note:Serializable
{
    @PrimaryKey(autoGenerate = true)
    var id:Int=0;

    @ColumnInfo(name = "title")
    var title:String?=null
    @ColumnInfo(name = "date_time")
    var dateTime:String?=null

    @ColumnInfo(name = "note_text")
    var noteText:String?=null
    @ColumnInfo(name = "image_path")
    var imagePath:String?=null
    @ColumnInfo(name = "tempImage")
    var imageTemp:String?=null
    @ColumnInfo(name = "color")
    var color:String?=null
    @ColumnInfo(name = "reminder")
    var reminder:String?=null
    @ColumnInfo(name = "reminderId")
    var reminderId:Int?=null
    @ColumnInfo(name = "sortBy")
    var millis:Int?=null

    @NonNull
    override fun toString(): String {
        return "$title : $dateTime"
    }
}