//package com.android.mystickynotes.widgets
//
//import android.app.Activity
//import android.appwidget.AppWidgetManager
//import android.content.Context
//import android.content.Intent
//import android.os.Bundle
//import android.view.View
//import android.widget.EditText
//import com.vss.easynotes.R
//
//class NewAppWidgetConfigureActivity : Activity() {
//    private var appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID
//    private lateinit var appWidgetTitle: EditText
//    private lateinit var appWidgetSubtitle: EditText
//    private lateinit var appWidgetText: EditText
//    private var onClickListener = View.OnClickListener {
//        val context = this@NewAppWidgetConfigureActivity
//
//        // When the button is clicked, store the string locally
//        val widgetTitle = appWidgetTitle.text.toString()
//        val widgetSubtitle = appWidgetSubtitle.text.toString()
//        val widgetText = appWidgetText.text.toString()
//        saveTitlePref(context, appWidgetId, widgetTitle,widgetText)
//
//        // It is the responsibility of the configuration activity to update the app widget
//        val appWidgetManager = AppWidgetManager.getInstance(context)
//        updateAppWidget(context, appWidgetManager, appWidgetId)
//
//        // Make sure we pass back the original appWidgetId
//        val resultValue = Intent()
//        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
//        setResult(RESULT_OK, resultValue)
//        finish()
//    }
//
//    public override fun onCreate(icicle: Bundle?) {
//        super.onCreate(icicle)
//
//        // Set the result to CANCELED.  This will cause the widget host to cancel
//        // out of the widget placement if the user presses the back button.
//        setResult(RESULT_CANCELED)
//
//        setContentView(R.layout.new_app_widget_configure)
//        appWidgetTitle = findViewById<View>(R.id.appwidget_title) as EditText
//        appWidgetSubtitle = findViewById<View>(R.id.appwidget_subtitle) as EditText
//        appWidgetText = findViewById<View>(R.id.appwidget_text) as EditText
//        findViewById<View>(R.id.ll_widget_layout).setOnClickListener(onClickListener)
//
//        // Find the widget id from the intent.
//        val intent = intent
//        val extras = intent.extras
//        if (extras != null) {
//            appWidgetId = extras.getInt(
//                AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID
//            )
//        }
//
//        // If this activity was started with an intent without an app widget ID, finish with an error.
//        if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
//            finish()
//            return
//        }
//
//        appWidgetTitle.setText(loadTitlePref(this@NewAppWidgetConfigureActivity, appWidgetId))
//        appWidgetText.setText(loadTextPref(this@NewAppWidgetConfigureActivity, appWidgetId))
//    }
//
//}
//
//private const val PREFS_NAME = "com.android.mystickynotes.widgets.NewAppWidget"
//private const val PREF_PREFIX_KEY = "appwidget_"
//
//// Write the prefix to the SharedPreferences object for this widget
//internal fun saveTitlePref(context: Context, appWidgetId: Int, title: String ,text: String) {
//
//    val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
//    prefs.putString(PREF_PREFIX_KEY + appWidgetId, title)
//    prefs.putString("text$appWidgetId", text)
//    prefs.apply()
//}
//
//// Read the prefix from the SharedPreferences object for this widget.
//// If there is no preference saved, get the default from a resource
//internal fun loadTitlePref(context: Context, appWidgetId: Int): String {
//    val prefs = context.getSharedPreferences(PREFS_NAME, 0)
//    val titleValue = prefs.getString(PREF_PREFIX_KEY + appWidgetId, null)
//    return titleValue ?: context.getString(R.string.appwidget_text)
//}
//internal fun loadTextPref(context: Context, appWidgetId: Int): String {
//    val prefs = context.getSharedPreferences(PREFS_NAME, 0)
//    val text = prefs.getString("text$appWidgetId", null)
//    return text ?: context.getString(R.string.appwidget_text)
//}
//
//internal fun deleteTitlePref(context: Context, appWidgetId: Int) {
//    val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
//    prefs.remove(PREF_PREFIX_KEY + appWidgetId)
//    prefs.apply()
//}
//
//internal fun deleteTextPref(context: Context, appWidgetId: Int) {
//    val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
//    prefs.remove("text$appWidgetId")
//    prefs.apply()
//}