package com.vss.easynotes.widgets

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.vss.easynotes.AlarmReceiver
import com.vss.easynotes.R
import com.vss.easynotes.database.NoteDatabase
import com.vss.easynotes.entities.Note
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.layout_miscellaneous.*
import kotlinx.android.synthetic.main.new_app_widget_configure.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class CreateWidgetNoteActivity : AppCompatActivity(), CoroutineScope {
    private var appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID
    private lateinit var appWidgetTitle: EditText
    private lateinit var appWidgetSubtitle: EditText
    private lateinit var appWidgetText: EditText

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main
    private var selectedNoteColor: String? = null
    private var viewSubtitle: View? = null
    private val REQUEST_PERMISSION = 100
    var selectedMediaPath: String? = null
    var selectedDate: String? = null
    var selectedTime: String? = null
    var selectedTimeRaw: String? = null
    var targetedDate: String? = null
     var selectedHour:Int?=null
    var alarmIntent:PendingIntent?=null
    var alarm:AlarmManager?=null
    var myIntent:Intent?=null
    var alarmId:Int?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setResult(Activity.RESULT_CANCELED)
        setContentView(R.layout.new_app_widget_configure)
        appWidgetTitle = findViewById<View>(R.id.appwidget_title) as EditText
        appWidgetSubtitle = findViewById<View>(R.id.appwidget_subtitle) as EditText
        appWidgetText = findViewById<View>(R.id.appwidget_text) as EditText

        // Find the widget id from the intent.
        val intent = intent
        val extras = intent.extras
        if (extras != null) {
            appWidgetId = extras.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID
            )
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish()
            return
        }

        appWidgetTitle.setText(loadTitlePref(this, appWidgetId))
        appWidgetText.setText(loadTextPref(this, appWidgetId))
        viewSubtitle = findViewById(R.id.viewSubtitleIndicator)
        job = Job()
        img_back.setOnClickListener {
            onBackPressed()
        }
        alarmId=System.currentTimeMillis().toInt()
        selectedNoteColor = "#333333"
        selectedMediaPath = ""
        targetedDate=""
        selectedTime=""
        text_date_time.text =
            SimpleDateFormat("EEEE , dd MMMM yyyy HH:mm a", Locale.getDefault()).format(Date())

        img_save.setOnClickListener {
            val context = this

            // When the button is clicked, store the string locally
            val widgetTitle = appWidgetTitle.text.toString()
            val widgetSubtitle = appWidgetSubtitle.text.toString()
            val widgetText = appWidgetText.text.toString()
            saveTitlePref(context, appWidgetId, widgetTitle,widgetText)

            // It is the responsibility of the configuration activity to update the app widget
            val appWidgetManager = AppWidgetManager.getInstance(context)
            updateAppWidget(context, appWidgetManager, appWidgetId)

            // Make sure we pass back the original appWidgetId
            val resultValue = Intent()
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            setResult(Activity.RESULT_OK, resultValue)

            saveNote()

        }

        alarm =  getSystemService(Context.ALARM_SERVICE) as AlarmManager

        deleteNoteImage.setOnClickListener {
            noteImage.setImageBitmap(null)
            noteImage.visibility = View.GONE
            deleteNoteImage.visibility = View.GONE
            selectedMediaPath = ""
        }
        ll_add_reminder_layout.setOnClickListener {
            setAlarm()
        }

        
        img_delete_reminder.setOnClickListener {
            ll_reminder_layout.visibility = View.GONE
            targetedDate=""
            selectedTime=""
            text_reminder.text=""
        }
        initBottomSheet()
        setSubtitleIndicator()
    }

    private fun setAlarm() {
        datePicker()
    }

    @SuppressLint("SimpleDateFormat")
    private fun datePicker() {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                selectedDate = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)

                val dateFormat = SimpleDateFormat("dd-M-yyyy")
                var sourceDate: Date? = null
                try {
                    sourceDate = dateFormat.parse(selectedDate!!)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                val targetFormat = SimpleDateFormat("MMMM dd")
                targetedDate = targetFormat.format(sourceDate!!)

                if (selectedDate != null) {
                    timePicker()
                }
            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }

    @SuppressLint("SimpleDateFormat")
    private fun timePicker() {
        var am_pm:String?=null
        val mCurrentTime = Calendar.getInstance()
        val hour = mCurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mCurrentTime.get(Calendar.MINUTE)
        if (mCurrentTime.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM";
        else if (mCurrentTime.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM";
      val  timePickerDialog = TimePickerDialog(this,
          TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
              selectedTimeRaw="$hourOfDay:$minute"
              when {
                  hourOfDay == 0 -> {
                      selectedHour = hourOfDay+12
                      am_pm = "AM"
                  }
                  hourOfDay == 12 -> {
                      am_pm = "PM"
                  }
                  hourOfDay > 12 -> {
                      selectedHour = hourOfDay-12
                      am_pm = "PM"
                  }
                  else -> {
                      am_pm = "AM"
                  }
              }
              selectedTime = "$selectedHour:$minute $am_pm"
              if (selectedTime != null) {

                  ll_reminder_layout.visibility = View.VISIBLE
                  text_reminder.text= "$targetedDate, $selectedTime"
              }
          }, hour, minute, false)
        timePickerDialog.show()

    }

    @SuppressLint("SimpleDateFormat")
    private fun convertDateTimeToMillis() {
        val date = "$selectedDate $selectedTimeRaw"//"2014-11-25 14:30"
        val sdf = SimpleDateFormat("dd-M-yyyy HH:mm")
        var testDate:Date? = null

        try
        {
            myIntent =  Intent(applicationContext, AlarmReceiver::class.java)
            myIntent!!.putExtra("notificationId", 1)
            myIntent!!.putExtra("todo", appWidgetTitle.text.toString())
            alarmIntent = PendingIntent.getBroadcast(applicationContext, alarmId!!, myIntent, 0)
            testDate = sdf.parse(date)
            alarm!!.set(AlarmManager.RTC, testDate.time, alarmIntent);

        }
        catch (ex:Exception) {
            ex.printStackTrace()
        }
    }

    private fun saveNote() {
        val title = appWidgetTitle.text.toString()
        val subtitle = appWidgetSubtitle.text.toString()
        val note = appWidgetText.text.toString()
        if (title.trim().isEmpty()) {
            Toast.makeText(this, "Title Required", Toast.LENGTH_SHORT).show()
            return
        }
        if (subtitle.trim().isEmpty()) {

            Toast.makeText(this, "Subtitle Required", Toast.LENGTH_SHORT).show()
            return
        }
        if (note.trim().isEmpty()) {
            Toast.makeText(this, "Note Required", Toast.LENGTH_SHORT).show()
            return
        }

        launch {

            let {
                val mNote = Note()
                mNote.title = title
                mNote.noteText = note
                mNote.dateTime = text_date_time.text.toString()
                mNote.color = selectedNoteColor
                mNote.imagePath = selectedMediaPath
                mNote.reminder= text_reminder.text.toString()
                mNote.reminderId=alarmId
                if (text_reminder.text.toString().length>1)
                {
                convertDateTimeToMillis()
                }
                    NoteDatabase(applicationContext).getNoteDao().insertNote(mNote)
                Toast.makeText(applicationContext, "Saved", Toast.LENGTH_SHORT).show()
                setResult(Activity.RESULT_OK)
                finish()

            }
        }
    }

    private fun initBottomSheet() {
        val chooseTheme = findViewById<LinearLayout>(R.id.ll_miscellaneous_layout)
        val bottomSheetBehavior = BottomSheetBehavior.from(chooseTheme)
        bottomSheetBehavior.peekHeight = 120
        chooseTheme.findViewById<TextView>(R.id.choose_theme).setOnClickListener {
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//                val view = this.currentFocus
//                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                imm.hideSoftInputFromWindow(view!!.windowToken, 0)
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        ll_add_image_layout.setOnClickListener {
            setupPermissions()
        }
        viewColor1.setOnClickListener {
            selectedNoteColor = "#333333"
            imgColor1.setImageResource(R.drawable.ic_done)
            imgColor2.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor5.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor2.setOnClickListener {
            selectedNoteColor = "#fdbe3b"
            imgColor2.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor5.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor3.setOnClickListener {
            selectedNoteColor = "#ff4842"
            imgColor3.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor2.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor5.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor4.setOnClickListener {
            selectedNoteColor = "#3a52fc"
            imgColor4.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor2.setImageResource(0)
            imgColor3.setImageResource(0)
            imgColor5.setImageResource(0)
            setSubtitleIndicator()
        }
        viewColor5.setOnClickListener {
            selectedNoteColor = "#000000"
            imgColor5.setImageResource(R.drawable.ic_done)
            imgColor1.setImageResource(0)
            imgColor2.setImageResource(0)
            imgColor4.setImageResource(0)
            imgColor3.setImageResource(0)
            setSubtitleIndicator()
        }

    }

    private fun setSubtitleIndicator() {
        val gradientDrawable = viewSubtitle!!.background as GradientDrawable
        gradientDrawable.setColor(Color.parseColor(selectedNoteColor))
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_PERMISSION
            )
        } else {
            selectImage()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSION -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("TAG", "Permission has been denied by user")
                } else {
                    selectImage()
                }
            }
        }
    }

    private fun selectImage() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        //intent.type = "image/*"
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), 2)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 2) {
                if (data != null) {
                    deleteNoteImage.visibility = View.VISIBLE
                    val image = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, image);
                    noteImage.setImageBitmap(bitmap)
                    noteImage.visibility = View.VISIBLE
                    val projection = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = contentResolver.query(image!!, projection, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()

                    val columnIndex = cursor.getColumnIndex(projection[0])
                    val mediaPath = cursor.getString(columnIndex)
                    selectedMediaPath = mediaPath
                    cursor.close()

                }
            } else if (requestCode == 1) {
                if (Build.VERSION.SDK_INT > 21) {
//                    setUpImojiIcon(mImageFileLocation)

                } else {
//                    setUpImojiIcon(fileUri)

                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}

private const val PREFS_NAME = "com.android.mystickynotes.widgets.NewAppWidget"
private const val PREF_PREFIX_KEY = "appwidget_"

// Write the prefix to the SharedPreferences object for this widget
internal fun saveTitlePref(context: Context, appWidgetId: Int, title: String ,text: String) {

    val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
    prefs.putString(PREF_PREFIX_KEY + appWidgetId, title)
    prefs.putString("text$appWidgetId", text)
    prefs.apply()
}

// Read the prefix from the SharedPreferences object for this widget.
// If there is no preference saved, get the default from a resource
internal fun loadTitlePref(context: Context, appWidgetId: Int): String {
    val prefs = context.getSharedPreferences(PREFS_NAME, 0)
    val titleValue = prefs.getString(PREF_PREFIX_KEY + appWidgetId, null)
    return titleValue ?: context.getString(R.string.appwidget_text)
}
internal fun loadTextPref(context: Context, appWidgetId: Int): String {
    val prefs = context.getSharedPreferences(PREFS_NAME, 0)
    val text = prefs.getString("text$appWidgetId", null)
    return text ?: context.getString(R.string.appwidget_text)
}

internal fun deleteTitlePref(context: Context, appWidgetId: Int) {
    val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
    prefs.remove(PREF_PREFIX_KEY + appWidgetId)
    prefs.apply()
}

internal fun deleteTextPref(context: Context, appWidgetId: Int) {
    val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
    prefs.remove("text$appWidgetId")
    prefs.apply()
}