package com.vss.easynotes

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.vss.easynotes.R
import com.vss.easynotes.activities.MainActivity
import java.util.*


class AlarmReceiver:BroadcastReceiver() {
   override fun onReceive(context:Context, intent:Intent) {
        // Get id & message from intent.
        val notificationId = intent.getIntExtra("notificationId", 0)
        val notificationId2 = intent.getIntExtra("notificationId2", 0)
        val message = intent.getStringExtra("todo")
        // When notification is tapped, call MainActivity.

    val builder = NotificationCompat.Builder(context, notificationId.toString())
       val mainIntent = Intent(context, MainActivity::class.java)
       val contentIntent = PendingIntent.getActivity(context, 0, mainIntent, 0)
        val inboxStyle = NotificationCompat.InboxStyle()
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val icon = R.drawable.icons_notification_96
       val bitmap = BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher)
        val notification = builder.setSmallIcon(icon).setTicker(message).setWhen(0)
            .setAutoCancel(true)
            .setContentTitle(message)
            .setContentIntent(contentIntent)
            .setStyle(inboxStyle)
            .setColor(Color.RED)
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .setLights(Color.RED, 3000, 3000)
            .setSound(soundUri)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setSmallIcon(icon)
            .setLargeIcon(bitmap)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
            .build()

        val notificationManager = NotificationManagerCompat.from(context)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            val channel =
                NotificationChannel(notificationId.toString(), "Default channel", NotificationManager.IMPORTANCE_HIGH)
            channel.setSound(soundUri, audioAttributes)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(generateRandom(), notification)

    }
    private fun generateRandom(): Int {
        val rn = Random()
        val n = 999999999 - 111111111 + 1
        val i = rn.nextInt() % n
        return 111111111 + i
    }
}