package com.vss.easynotes.interfaces

import com.vss.easynotes.entities.Note

interface NotesListener {
    fun onNoteClicked(note: Note, position:Int)
}